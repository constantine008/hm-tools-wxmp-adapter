package top.hmtools.wxmp.message.group.model.preview;

public class ImagePreviewParam extends BasePreviewParam {

	private MediaId image;

	public MediaId getImage() {
		return image;
	}

	public void setImage(MediaId image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ImagePreviewParam [image=" + image + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
