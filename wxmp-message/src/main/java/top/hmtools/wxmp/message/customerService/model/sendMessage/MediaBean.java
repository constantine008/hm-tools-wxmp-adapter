package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * 消息管理 之 客服消息 之 媒体消息（图片、语音、视频 的基础类）
 * @author hybo
 *
 */
public class MediaBean {

	/**
	 * 媒体数据ID
	 */
	protected String media_id;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	
	
}
