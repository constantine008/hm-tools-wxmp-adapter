package top.hmtools.wxmp.message.customerService.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;
import top.hmtools.wxmp.message.customerService.model.KfAccountListResult;
import top.hmtools.wxmp.message.customerService.model.KfAccountParam;
import top.hmtools.wxmp.message.customerService.model.TypingStatusParam;
import top.hmtools.wxmp.message.customerService.model.UploadHeadImgParam;

@WxmpMapper
public interface ICustomerServiceApi {

	/**
	 * 添加客服帐号
	 * @param kfAccountParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/customservice/kfaccount/add")
	public ErrcodeBean addKfAcount(KfAccountParam kfAccountParam);

	/**
	 * 修改客服账号
	 * @param kfAccountParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/customservice/kfaccount/update")
	public ErrcodeBean updateKfAcount(KfAccountParam kfAccountParam);

	/**
	 * 删除客服帐号
	 * @param kfAccountParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/customservice/kfaccount/del")
	public ErrcodeBean deleteKfAcount(KfAccountParam kfAccountParam);
	
	/**
	 * 设置客服帐号的头像
	 * @param uploadHeadImgParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/customservice/kfaccount/uploadheadimg",httpParamType=HttpParamType.FORM_DATA)
	public ErrcodeBean uploadHeadImg(UploadHeadImgParam uploadHeadImgParam);
	
	/**
	 * 获取所有客服账号
	 * <br>开发者通过本接口，获取公众号中所设置的客服基本信息，包括客服工号、客服昵称、客服登录账号。
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/customservice/getkflist")
	public KfAccountListResult getKfList();
	
	/**
	 * 发送消息
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/custom/send")
	public ErrcodeBean sendMessage(BaseSendMessageParam baseSendMessageParam);
	
	/**
	 * 客服输入状态
	 * @param typingStatusParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/custom/typing")
	public ErrcodeBean typingStatus(TypingStatusParam typingStatusParam);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
