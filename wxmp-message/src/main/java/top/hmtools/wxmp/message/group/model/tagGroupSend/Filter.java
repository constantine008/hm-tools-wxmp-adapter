package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * Auto-generated: 2019-08-26 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Filter {

	private boolean is_to_all;
	private int tag_id;

	public void setIs_to_all(boolean is_to_all) {
		this.is_to_all = is_to_all;
	}

	public boolean getIs_to_all() {
		return is_to_all;
	}

	public void setTag_id(int tag_id) {
		this.tag_id = tag_id;
	}

	public int getTag_id() {
		return tag_id;
	}

}