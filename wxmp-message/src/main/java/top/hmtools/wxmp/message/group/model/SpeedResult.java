package top.hmtools.wxmp.message.group.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class SpeedResult extends ErrcodeBean {

	private int speed;
	private int realspeed;

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getRealspeed() {
		return realspeed;
	}

	public void setRealspeed(int realspeed) {
		this.realspeed = realspeed;
	}

	@Override
	public String toString() {
		return "SpeedResult [speed=" + speed + ", realspeed=" + realspeed + ", errcode=" + errcode + ", errmsg="
				+ errmsg + "]";
	}

}
