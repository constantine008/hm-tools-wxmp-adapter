package top.hmtools.wxmp.message.eventPush.model;

import com.thoughtworks.xstream.XStream;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 自定义菜单事件 之  点击菜单拉取消息时的事件推送
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[FromUser]]></FromUserName>
  <CreateTime>123456789</CreateTime>
  <MsgType><![CDATA[event]]></MsgType>
  <Event><![CDATA[CLICK]]></Event>
  <EventKey><![CDATA[EVENTKEY]]></EventKey>
</xml>
 * }
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.CLICK)
public class ClickMenuEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1160352525724951725L;

	@Override
	public void configXStream(XStream xStream) {
		
	}


	
}
