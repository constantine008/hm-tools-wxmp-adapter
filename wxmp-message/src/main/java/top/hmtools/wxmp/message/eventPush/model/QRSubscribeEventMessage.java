package top.hmtools.wxmp.message.eventPush.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 扫描带参数二维码事件
 * <br>之  1. 用户未关注时，进行关注后的事件推送
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[FromUser]]></FromUserName>
	<CreateTime>123456789</CreateTime>
	<MsgType><![CDATA[event]]></MsgType>
	<Event><![CDATA[subscribe]]></Event>
	<EventKey><![CDATA[qrscene_123123]]></EventKey>
	<Ticket><![CDATA[TICKET]]></Ticket>
</xml>
 * }
 * @author Hybomyth
 *
 */
//@WxmpMessage(msgType=MsgType.event,event=Event.subscribe)
public class QRSubscribeEventMessage extends SubscribeEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3281721328833280247L;

	/**
	 * 二维码的ticket，可用来换取二维码图片
	 */
	@XStreamAlias("Ticket")
	private String ticket;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "QRSubscribeEventMessage [ticket=" + ticket + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}
	
	
}
