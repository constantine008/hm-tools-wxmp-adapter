package top.hmtools.wxmp.material.apis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSON;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.material.enums.MediaType;
import top.hmtools.wxmp.material.model.Articles;
import top.hmtools.wxmp.material.model.BatchgetMaterialParam;
import top.hmtools.wxmp.material.model.BatchgetMaterialResult;
import top.hmtools.wxmp.material.model.DescriptionBean;
import top.hmtools.wxmp.material.model.ItemResult;
import top.hmtools.wxmp.material.model.MaterialCountResult;
import top.hmtools.wxmp.material.model.MaterialResult;
import top.hmtools.wxmp.material.model.MediaBean;
import top.hmtools.wxmp.material.model.MediaParam;
import top.hmtools.wxmp.material.model.NewsBean;
import top.hmtools.wxmp.material.model.NewsBeanForUpdate;
import top.hmtools.wxmp.material.model.UploadParam;

public class IForeverApiTest {

	protected WxmpSession wxmpSession;

	IForeverApi foreverApi;

	@Before
	public void init() {
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		foreverApi = this.wxmpSession.getMapper(IForeverApi.class);
	}

	@Test
	public void testAddNews() {
		List<Articles> articles = new ArrayList<Articles>();
		
		Articles one = new Articles();
		one.setAuthor("hybo");
		one.setContent("hhhhhhhhhhhhhhhh");
		one.setContent_source_url("http://www.baidu.com");
		one.setDigest("aaaaaaaaa");
		one.setNeed_open_comment(1);
		one.setOnly_fans_can_comment(0);
		one.setShow_cover_pic(1);
		one.setThumb_media_id("p2nPaor93V7DCUL8gyiMwK5gBjFN0DDnjP8AzK_HuO8");
		one.setTitle("test aaa");
		
		articles.add(one);
		
		NewsBean newsBean = new NewsBean();
		newsBean.setArticles(articles);
		MediaBean mediaBean = this.foreverApi.addNews(newsBean);
		System.out.println(mediaBean);
		//MediaBean [type=null, media_id=p2nPaor93V7DCUL8gyiMwNGlnR-VnavyFDYVc4_6-zo, url=null, created_at=0, errcode=0, errmsg=null]

	}

	@Test
	public void testUploadImageForNews() {
		File file = new File("E:\\tmp\\aaaa.png");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		MediaBean uploadImageForNews = this.foreverApi.uploadImageForNews(uploadParam);
		System.out.println(uploadImageForNews);
	}

	@Test
	public void testAddMaterialThumb() {
		File file = new File("E:\\tmp\\aaaa.png");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.thumb);
		MediaBean mediaBean = this.foreverApi.addMaterial(uploadParam);
		System.out.println(mediaBean);
		//MediaBean [type=null, media_id=p2nPaor93V7DCUL8gyiMwK5gBjFN0DDnjP8AzK_HuO8, url=http://mmbiz.qpic.cn/mmbiz_png/MniahHjhIESYAmoSicWxDDfs7aLC1fSGWuibsOuMD5bR4RWPPcFvf4yVvvneg8WOzuicthHWENx5NoJ3JXJfQhVUcA/0?wx_fmt=png, created_at=0, errcode=0, errmsg=null]

	}
	
	@Test
	public void testAddMaterialVideo() {
		File file = new File("E:\\tmp\\bbb.mp4");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.video);
		DescriptionBean descriptionBean = new DescriptionBean();
		descriptionBean.setIntroduction("test video");
		descriptionBean.setTitle("e~~~~");
		
		String description = JSON.toJSONString(descriptionBean);
		uploadParam.setDescription(description);
		MediaBean mediaBean = this.foreverApi.addMaterial(uploadParam);
		System.out.println(mediaBean);
		//MediaBean [type=null, media_id=p2nPaor93V7DCUL8gyiMwMUBvpMlh5pbX9Uv4yWH4b4, url=null, created_at=0, errcode=0, errmsg=null]
	}
	
	@Test
	public void getMaterial(){
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id("p2nPaor93V7DCUL8gyiMwMUBvpMlh5pbX9Uv4yWH4b4");
		MaterialResult material = this.foreverApi.getMaterial(mediaParam);
		System.out.println(material);
		//MaterialResult [title=e~~~~, description=test video, down_url=http://203.205.158.83/vweixinp.tc.qq.com/1007_55ae62517b164913a9141668e8b0baf5.f10.mp4?vkey=B8E15EA6A2C432190C5775940F7C43EA1D1D546CB4428083C79EB586569BB58BD4FB6869EB6F548DA1CCBECDCA2EF3B18AD0737DAFF6352AB4CB4EEC8B5664DD8A0C32EE0D334FB35FC94C1DCDDACDFBCD5825825B6C8FD9&sha=0&save=1, news_item=null, errcode=0, errmsg=null]

		mediaParam.setMedia_id("p2nPaor93V7DCUL8gyiMwNGlnR-VnavyFDYVc4_6-zo");
		material = this.foreverApi.getMaterial(mediaParam);
		System.out.println(material);
	}
	
	
	@Test
	public void delMaterial(){
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id("p2nPaor93V7DCUL8gyiMwK5gBjFN0DDnjP8AzK_HuO8");
		ErrcodeBean delMaterial = this.foreverApi.delMaterial(mediaParam);
		System.out.println(delMaterial);
	}
	
	@Test
	public void updateNews(){
		BatchgetMaterialParam batchgetMaterialParam = new BatchgetMaterialParam(MediaType.news, 0, 20);
		BatchgetMaterialResult batchgetMaterial = this.foreverApi.getBatchgetMaterial(batchgetMaterialParam);
		ItemResult itemResult = batchgetMaterial.getItem().get(1);
		Articles articles =itemResult.getContent().getNews_item().get(0);
		
		
		NewsBeanForUpdate newsBean = new NewsBeanForUpdate();
		newsBean.setMedia_id(itemResult.getMedia_id());
		newsBean.setArticles(articles);
		newsBean.setIndex(0);
		this.foreverApi.updateNews(newsBean);
	}
	
	@Test
	public void getMaterialCount(){
		MaterialCountResult materialCount = this.foreverApi.getMaterialCount();
		System.out.println(materialCount);
	}
	
	@Test
	public void getBatchgetMaterial(){
		BatchgetMaterialParam batchgetMaterialParam = new BatchgetMaterialParam(MediaType.news, 0, 20);
		BatchgetMaterialResult batchgetMaterial = this.foreverApi.getBatchgetMaterial(batchgetMaterialParam);
		System.out.println(batchgetMaterial);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
