package top.hmtools.wxmp.webpage.jsSdk;

/**
 * 所有需要使用JS-SDK的页面必须先注入配置信息，否则将无法调用（同一个url仅需调用一次，对于变化url的SPA的web app可在每次url变化时进行调用,目前Android微信客户端不支持pushState的H5新特性，所以使用pushState来实现web app的页面会导致签名失败，此问题会在Android6.2中修复）。
 * <br>本实体类用于后端服务生成签名，并组装成其config对象数据实例，便于前端页面js初始化
 * @author Hybomyth
 *
 */
public class JsConfigVO {

	/**
	 * 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
	 */
	private boolean debug;
	
	/**
	 * 必填，公众号的唯一标识
	 */
	private String appId;
	
	/**
	 * 必填，生成签名的时间戳
	 * <br>精度单位是：秒
	 */
	private Long timestamp;
	
	/**
	 * 必填，生成签名的随机串
	 */
	private String nonceStr;
	
	/**
	 * 必填，签名
	 */
	private String signature;
	
	/**
	 * 参与签名内容的URL字符串
	 */
	private String urlInSignature;
	
	/**
	 * 原始URL字符串
	 */
	private String url;

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getUrlInSignature() {
		return urlInSignature;
	}

	public void setUrlInSignature(String urlInSignature) {
		this.urlInSignature = urlInSignature;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "JsConfigVO [debug=" + debug + ", appId=" + appId + ", timestamp=" + timestamp + ", nonceStr=" + nonceStr
				+ ", signature=" + signature + ", urlInSignature=" + urlInSignature + ", url=" + url + "]";
	}
	
	
}
