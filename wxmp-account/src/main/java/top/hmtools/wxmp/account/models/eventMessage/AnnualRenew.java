package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * 年审通知
 * {@code 
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442401004</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[annual_renew]]></Event>  
  <ExpiredTime>1442401004</ExpiredTime> 
</xml>
 * }
 * @author HyboWork
 *
 */
public class AnnualRenew extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8313660158036362807L;
	
	@XStreamAlias("ExpiredTime")
	private Long expiredTime;
	
	

	public Long getExpiredTime() {
		return expiredTime;
	}



	public void setExpiredTime(Long expiredTime) {
		this.expiredTime = expiredTime;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "AnnualRenew [expiredTime=" + expiredTime + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
