#### 前言
本组件对应实现微信公众平台“用户管理”章节相关api接口，原接口文档地址：[用户管理](https://developers.weixin.qq.com/doc/offiaccount/User_Management/User_Tag_Management.html)

#### 接口说明
- `top.hmtools.wxmp.user.apis.IBlackListApi` 对应 黑名单管理
- `top.hmtools.wxmp.user.apis.IRemarkApi` 对应 设置用户备注名
- `top.hmtools.wxmp.user.apis.ITagsApi` 对应 用户标签管理
- `top.hmtools.wxmp.user.apis.IUnionIDApi` 对应 获取用户基本信息(UnionID机制)
- `top.hmtools.wxmp.user.apis.IUserListApi` 对应获取用户列表

#### 事件消息类说明
- `top.hmtools.wxmp.user.model.eventMessage` 包对应 获取用户地理位置 消息通知。

#### 使用示例
```
```
更多示例参见：
- [黑名单管理](src/test/java/top/hmtools/wxmp/user/apis/IBlackListApiTest.java)
- [设置用户备注名](src/test/java/top/hmtools/wxmp/user/apis/IRemarkApiTest.java)
- [用户标签管理](src/test/java/top/hmtools/wxmp/user/apis/ITagsApiTest.java)
- [获取用户基本信息(UnionID机制)](src/test/java/top/hmtools/wxmp/user/apis/IUnionIDApiTest.java)
- [获取用户列表](src/test/java/top/hmtools/wxmp/user/apis/IUserListApiTest.java)
