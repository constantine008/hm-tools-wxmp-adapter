package top.hmtools.wxmp.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;
import top.hmtools.wxmp.core.eventModels.NamingVerifyFail;
import top.hmtools.wxmp.core.eventModels.NamingVerifySuccess;
import top.hmtools.wxmp.core.eventModels.TextMessage;

@WxmpController
public class WxmpControllerTest {
	
	final Logger logger = LoggerFactory.getLogger(WxmpControllerTest.class);

	@WxmpRequestMapping
	public String executeTextMessage(TextMessage textMessage){
		this.logger.info("获取的入参内容是：{}",textMessage);
		return "top.hmtools.wxmp.core.WxmpControllerTest.executeTextMessage(TextMessage) 执行OK";
	}
	
	@WxmpRequestMapping
	public NamingVerifySuccess executeNamingVerifySuccess(NamingVerifySuccess namingVerifySuccess){
		this.logger.info("获取的入参内容是：{}",namingVerifySuccess);
		return namingVerifySuccess;
	}
	
	@WxmpRequestMapping
	public NamingVerifyFail executeNamingVerifyFail(NamingVerifyFail namingVerifyFail){
		this.logger.info("获取的入参内容是：{}",namingVerifyFail);
		return namingVerifyFail;
	}
}
