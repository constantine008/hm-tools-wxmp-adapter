package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpApi {

	HttpMethods httpMethods() default HttpMethods.POST;
	
	HttpParamType httpParamType() default HttpParamType.JSON_BODY;
	
	@AliasFor("value")
	String uri();
	
	@AliasFor("uri")
	String value() default "";
}
