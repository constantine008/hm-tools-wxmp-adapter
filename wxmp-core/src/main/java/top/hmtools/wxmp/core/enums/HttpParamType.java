package top.hmtools.wxmp.core.enums;

public enum HttpParamType {

	FORM_DATA,
	JSON_BODY;
}
